#libraries 
install.packages('data.table')
library(data.table)


#handle csv files
#process documents_categories
cat<-fread(file=unzip( "documents_categories.csv.zip"))
nrow(cat)
head(cat,n=5000000)
cat1<-unique(cat[order(-confidence_level)],by='document_id')
cat1$category_id<-as.factor(cat1$category_id)
cat2<-na.omit(cat1)
rm(cat,cat1)
gc()

#process documents_topics
top<-fread(file=unzip( "documents_topics.csv.zip"))
head(top,n=5000000)
top1<-unique(top[order(-confidence_level)],by='document_id')
top1$topic_id<-as.factor(top1$topic_id)
top2<-na.omit(top1)
rm(top,top1)
gc()

#process documents_entities
ent<-fread(file=unzip( "documents_entities.csv.zip"))
head(ent,n=50)
ent1<-unique(ent[order(-confidence_level)],by='document_id')
ent1$entity_id<-as.factor(ent1$entity_id)
ent2<-na.omit(ent1)
rm(ent,ent1)
gc()


#process documents_meta
meta<-fread(file=unzip( "documents_meta.csv.zip"))
head(meta,n=5000000)
meta$source_id<-as.factor(meta$source_id)
meta$publisher_id<-as.factor(meta$publisher_id)
meta1<-na.omit(meta)
rm(meta)
gc()

#process events
events<- fread(file=unzip( "events.csv.zip"))
head(events,n=50)
convert_platform<-function(x){ #convert platform to factor
  ifelse(x==1,'desktop',ifelse(x==2,'mobile','tablet'))
  
}
events$platform<-sapply(events$platform,convert_platform)


#process page-view-sample
#pv<-fread(file=unzip( "page_views_sample.csv.zip"))
#head(pv)
#pv$platform<-sapply(pv$platform,convert_platform)
#pv$uuid<-as.factor(pv$uuid)
#str(pv)

#merging datasets with document_id as key
ds1<-merge(events,top2,by='document_id')
head(ds1)
ds2<-merge(ds1,meta1,by='document_id')
head(ds2)
rm(ds1)
ds3<-merge(ds2,cat2,by='document_id')
head(ds3)
rm(ds2)
ds4<-merge(ds3,ent2,by='document_id')
head(ds4)
rm(ds3,meta1,cat2,ent2,events,pro,top2)
gc()
#process promoted_content

#merging all tables
install.packages('ff')
library(ff)


install.packages('ffbase')
library(ffbase)
require(ffbase)

write.csv(ds4, file = "ds4.csv")
ds4<-read.csv.ffdf(file='ds4.csv',colClass=NULL)
train <- read.csv.ffdf(file=unzip( "clicks_train.csv.zip"))
head(train)
ds5<-merge(train,ds4,by='display_id')
head(ds5)
nrow(ds4)
pro<-read.csv.ffdf(file=unzip( "promoted_content.csv.zip"))
head(pro)
ds6<-merge(ds5,pro,by='ad_id')
str(ds6)
head(ds6)
nrow(ds6)
ds6<-as.data.frame(ds6)

ds6<-ds6[1:1500000,]

ds6$platform<-sapply(ds6$platform,convert_platform)
ds6$publisher_id<-as.factor(ds6$publisher_id)
ds6$publish_time<-as.numeric(ds6$publish_time)

#removing undeeded attributes
unwanted<-c('uuid','confidence_level','confidence_level.x','confidence_level.y','publish_time','timestamp','X','document_id.y')
ds7<-ds6
ds7[,unwanted]<-NULL


#changing list types
ds7$display_id<-as.factor(ds7$display_id)
ds7$entity_id<-as.factor(ds7$entity_id)
ds7$topic_id<-as.factor(ds7$topic_id)
ds7$category_id<-as.factor(ds7$category_id)
ds7$platform<-as.factor(ds7$platform)
ds7$advertiser_id<-as.factor(ds7$advertiser_id)
ds7$source_id<-as.factor(ds7$source_id)
ds7$ad_id<-as.factor(ds7$ad_id)
ds7$clicked<-as.factor(ds7$clicked)
ds7$campaign_id<-as.factor(ds7$campaign_id)
ds7$document_id.x<-as.factor(ds7$document_id.x)

#deviding into test and train set
uniqueId<-as.data.frame(unique(ds7$display_id))
nrow(uniqueId)
set.seed(123)
split <- runif(nrow(uniqueId))>0.3
#split to train set and test set
train_df <- uniqueId[split,]
test_df<- uniqueId[!split,]

rm(uniqueId,split)
gc()

train<-ds7[ds7$display_id %in% train_df,]
test<-ds7[ds7$display_id %in% test_df,]
head(train)

#----------------------------------------------------------MAP----------------------------------------------------------------------------------------------------------------------------------
#test[,c('rowCount')]<-1#to help count number of ads in displays
#countDisplay<-aggregate.data.frame(test[,c('rowCount')],by=list(test[,c('display_id')]),length)#count number of ads in displays
#whichDisplay<-which(test$clicked==1)#find indexes where ad was clicked 
#head(countDisplay,n=7)
#head(test,n=40)
#nrow(countDisplay)
#nrow(whichDisplay)
#countDisplay[,c('index')]<-whichDisplay
#install.packages('dplyr')
#library(dplyr)

#countDisplay<-countDisplay %>% mutate(cumsum = cumsum(x))#accumulate 
#countDisplay[2:90353,4]<-countDisplay$cumsum# putting accumualt in front of index to find index by group 
#countDisplay[1,4]<-0# put 0 in first row
#countDisplay$real<-countDisplay$index-countDisplay$cumsum# find average precision
#ap<-1/countDisplay$real
#head(ap)

#find map
#ap1<-na.omit(ap)
#sum(ap1)
#ap1<-as.data.frame(ap1)
#testMap<-sum(ap1)/nrow(ap1)
test<-as.data.frame(test)
maptest = setDT(test)[ , .(map_12 = 1 / which(clicked == 1)), by = display_id][['map_12']]
mean(maptest)
#--------------------------------------------------------------- MODELS--------------------------------------------------------------------------------------------------
str(train)

#compute the decision tree model
install.packages('rpart')
library(rpart)

install.packages('rpart.plot')
library(rpart.plot)

dcModel <- rpart(clicked ~ .,
                 data=train,method="class")
rpart.plot::rpart.plot(dcModel,fallen.leaves = FALSE)


#test set
#preparing for computing the confusion matrix
predicted <- predict(dcModel, test [,-3],type = "prob")
test1<-test
test1[,c('index')]<-1:nrow(test1)
test1[,c('score')]<-predicted[,c('1')]
maxrows<-setDT(test1)[, c(.SD[which.max(score)]),display_id]
maxrows[,3:14]<-NULL
test1[maxrows$index,c('predict')]<-1

conv_na <- function(x){
  
  x <- ifelse ( is.na(x),0,1)
  
} 
 conv_10 <- function(x){
  
  x <- ifelse ( x == 1,1,0)
  
}
#converting won-loss to 1-0
test1$predict<-sapply(test1$predict,conv_na)
predicted01 <- sapply(test1$predict,conv_10)
actual01 <- sapply(test1$clicked,conv_10)

#computing confusion matrix
install.packages('SDMTools')
library(SDMTools)

confusion.matrix(actual01,predicted01 )


#naive bayse model
install.packages('e1071')
library(e1071)


model <- naiveBayes(train[,-3],train$clicked)

#test
predicted2<-predict(model, test[,-3],type="raw")
test2<-test
test2[,c('index')]<-1:nrow(test2)
test2[,c('score')]<-predicted2[,c('1')]
maxrows2<-setDT(test2)[, c(.SD[which.max(score)]),display_id]
maxrows2[,3:14]<-NULL
test2[maxrows2$index,c('predict')]<-1
typeof(maxrows2)

test2$predict<-sapply(test2$predict,conv_na)
pred02 <- sapply(test2$predict,conv_10)
actual02 <- sapply(test2$clicked,conv_10)

confusion.matrix(actual02,predicted02 )


#-----------------------------------------------MAPs and APs for models-----------------------------------------------------
install.packages('dplyr')
library(dplyr)

test1 <- test1 %>%
  group_by(display_id) %>%
  mutate(position = order(order(score, decreasing=TRUE)))

test2 <- test2 %>%
  group_by(display_id) %>%
  mutate(position = order(order(score, decreasing=TRUE)))


a = test1[which(test1$clicked==1),]
map1<-1/a$position
mean(map1)

b = test2[which(test2$clicked==1),]
map2<-1/b$position
mean(map2)


submissionFile<-test2[order(display_id,score),c(1,2)]
submissionFile<-submissionFile[,.(ad_id=paste(rev(ad_id),collapse=" ")),by=display_id]
write.csv(submissionFile,file="submissionFile.csv")
